CC = g++
CFLAGS = -Wall -std=c++11 -fopenmp

Singleton.o: Singleton.cpp Singleton.h
	$(CC) $(CFLAGS) -c -o $@ Singleton.cpp

task_1.o: task_1.cpp
	$(CC) $(CFLAGS) -c -o $@ $<

task_1: Singleton.o task_1.o
	$(CC) $(CFLAGS) -o $@ $^

task_2: task_2.cpp ThreadSafeQueue.h ThreadSafeQueueOptimized.h
	$(CC) $(CFLAGS) -o $@ task_2.cpp

all: task_1 task_2

clean:
	rm -f task_1 task_1.o Singleton.o task_2
	
