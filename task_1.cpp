#include <cstdio>
#include "Singleton.h"

int main()
{
	#pragma omp parallel
	{
		Singleton* singleton = Singleton::getInstance();
		printf("Singleton address: %p\n", singleton);
	}
}
