#include <deque>
#include <omp.h>

template<class Type>
class ThreadSafeQueueOptimized {
public:
	ThreadSafeQueueOptimized();

	void push(const Type& value);
	Type pop();
	void clear();
	unsigned int size();
	bool isEmpty() const;

private:
	std::deque<Type> queue;
	omp_lock_t pushLock;
	omp_lock_t popLock;
};

template<class Type>
ThreadSafeQueueOptimized<Type>::ThreadSafeQueueOptimized()
{
	omp_init_lock(&this->pushLock);
	omp_init_lock(&this->popLock);
}

template<class Type>
void ThreadSafeQueueOptimized<Type>::push(const Type& value)
{
	omp_set_lock(&this->pushLock);

	this->queue.push_back(value);

	omp_unset_lock(&this->pushLock);
}

template<class Type>
Type ThreadSafeQueueOptimized<Type>::pop()
{
	omp_set_lock(&this->popLock);
	
	Type value = this->queue.front();
	this->queue.pop_front();
	
	omp_unset_lock(&this->popLock);

	return value;
}

template<class Type>
unsigned int ThreadSafeQueueOptimized<Type>::size()
{
	omp_set_lock(&this->popLock);
	omp_set_lock(&this->pushLock);

	unsigned int size = this->queue.size();

	omp_unset_lock(&this->pushLock);
	omp_unset_lock(&this->popLock);
	
	return size;
}

template<class Type>
bool ThreadSafeQueueOptimized<Type>::isEmpty() const
{
	return this->queue.empty();
}

template<class Type>
void ThreadSafeQueueOptimized<Type>::clear()
{
	omp_set_lock(&this->popLock);
	omp_set_lock(&this->pushLock);
	
	this->queue.clear();
	
	omp_unset_lock(&this->pushLock);
	omp_unset_lock(&this->popLock);
}
