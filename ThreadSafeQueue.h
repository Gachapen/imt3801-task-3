#include <deque>

template<class Type>
class ThreadSafeQueue {
public:
	void push(const Type& value);
	Type pop();
	void clear();
	unsigned int size() const;
	bool isEmpty() const;

private:
	std::deque<Type> queue;
};

template<class Type>
void ThreadSafeQueue<Type>::push(const Type& value)
{
	#pragma omp critical
	{
		this->queue.push_back(value);
	}
}

template<class Type>
Type ThreadSafeQueue<Type>::pop()
{
	Type value;
	#pragma omp critical
	{
		value = this->queue.front();
		this->queue.pop_front();
	}

	return value;
}

template<class Type>
unsigned int ThreadSafeQueue<Type>::size() const
{
	unsigned int size = 0;
	#pragma omp critical
	{
		size = this->queue.size();
	}
	
	return size;
}

template<class Type>
bool ThreadSafeQueue<Type>::isEmpty() const
{
	return this->queue.empty();
}

template<class Type>
void ThreadSafeQueue<Type>::clear()
{
	#pragma omp critical
	{
		this->queue.clear();
	}
}
