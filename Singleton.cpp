#include "Singleton.h"

Singleton* Singleton::instance = nullptr;

Singleton* Singleton::getInstance()
{
	#pragma omp critical
	{
		if (instance == nullptr) {
			instance = new Singleton();
		}
	}

	return instance;
}

Singleton::Singleton()
{
}

Singleton::~Singleton()
{
}
