#include <cstdio>
#include <cstring>
#include <string>
#include "ThreadSafeQueue.h"
#include "ThreadSafeQueueOptimized.h"

void testPushing(const int NUM_ITEMS)
{
	ThreadSafeQueue<std::string> queue;

	#pragma omp parallel for
	for (int i = 0; i < NUM_ITEMS; i++) {
		queue.push(std::to_string(i));
	}

	if (queue.size() != (unsigned int)NUM_ITEMS) {
		printf("Incorrect queue size after pushes.\n");
	} else {
		printf("Correct queue size after pushes.\n");
	}

	printf("Queue size after push: %i (%i pushes).\n", queue.size(), NUM_ITEMS);

	queue.clear();
}

void testPopping(const int NUM_ITEMS)
{
	ThreadSafeQueue<std::string> queue;

	for (int i = 0; i < NUM_ITEMS; i++) {
		queue.push(std::to_string(i));
	}

	#pragma omp parallel for
	for (int i = 0; i < NUM_ITEMS; i++) {
		std::string value = queue.pop();
		printf("Popped %s\n", value.c_str());
	}
	
	printf("Queue size after pop: %i\n", queue.size());

	queue.clear();
}

void testParallelPushingAndPopping(const int NUM_ITEMS)
{
	ThreadSafeQueue<std::string> queue;

	#pragma omp parallel for
	for (int i = 0; i < NUM_ITEMS; i++) {
		queue.push(std::to_string(i));
		queue.pop();
	}

	printf("Queue size after popping and pushing: %i\n", queue.size());

	queue.clear();
}

void testParallelPushingAndPoppingOptimized(const int NUM_ITEMS)
{
	ThreadSafeQueueOptimized<std::string> queue;

	#pragma omp parallel for
	for (int i = 0; i < NUM_ITEMS; i++) {
		queue.push(std::to_string(i));
		queue.pop();
	}

	printf("Queue size after popping and pushing: %i\n", queue.size());

	queue.clear();
}

int main(int argc, char* argv[])
{
	const int NUM_ITEMS = 10000;

	bool testPush = false;
	bool testPop = false;
	bool testPopPush = false;

	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "push") == 0) {
			testPush = true;
		}

		if (strcmp(argv[i], "pop") == 0) {
			testPop = true;
		}

		if (strcmp(argv[i], "popush") == 0) {
			testPopPush = true;
		}
	}

	if (testPop == false && testPush == false && testPopPush == false) {
		testPush = true;
		testPop = true;
	}
	
	if (testPush == true) {
		printf("Testing queue push:\n");
		testPushing(NUM_ITEMS);
	}

	if (testPop == true) {
		printf("\n\nTesting queue pop:\n");
		testPopping(NUM_ITEMS);
	}
	
	if (testPopPush == true) {
		printf("\n\nTesting queue parallel pop and push:\n\n");
		
		double startTime = omp_get_wtime();
		testParallelPushingAndPopping(NUM_ITEMS);
		double endTime = omp_get_wtime();
		printf("Parallel pop and push used %f seconds.\n\n", endTime - startTime);

		startTime = omp_get_wtime();
		testParallelPushingAndPoppingOptimized(NUM_ITEMS);
		endTime = omp_get_wtime();
		printf("Parallel pop and push optimized used %f seconds.\n", endTime - startTime);
	}
}
